import FirFilter from "./index";

test("default", () => {
  const firFilter = new FirFilter([1, 0, 0, -1]);
  expect(firFilter.filter([1])).toStrictEqual([-1]);
  expect(firFilter.filter([2, 3])).toStrictEqual([-2, -3]);
  expect(firFilter.filter([4])).toStrictEqual([-3]);
});

test("reset", () => {
  const firFilter = new FirFilter([1, 0, 0, -1]);
  expect(firFilter.filter([1, 2, 3])).toStrictEqual([-1, -2, -3]);
  firFilter.reset();
  expect(firFilter.filter([4])).toStrictEqual([-4]);
});
