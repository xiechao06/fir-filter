export default class FirFilter {
  private coeffs: number[];
  private buffer: Float32Array;
  private ptr: number = 0;

  constructor(coeffs: number[]) {
    this.coeffs = coeffs;
    // buffer actually is a window
    this.buffer = new Float32Array(this._order);
    this.reset();
  }

  private get _order() {
    return this.coeffs.length;
  }
  /**
   * @param  {number[]} input
   * @returns number[]
   */
  public filter(input: number[]): number[] {
    const output = new Float32Array(input.length);
    for (let i = 0; i < input.length; ++i) {
      this.buffer[this.ptr] = input[i];
      // start from oldest to newest, sum from 0 to 10 equals to 10 to 0
      for (let j = 1; j <= this._order; ++j) {
        // idx actually point to the oldest value involed
        let idx = this.ptr + j;
        if (idx >= this._order) {
          idx -= this._order;
        }
        output[i] += this.buffer[idx] * this.coeffs[j - 1];
      }
      ++this.ptr;
      if (this.ptr >= this._order) {
        this.ptr -= this._order;
      }
    }
    return [].slice.call(output);
  }

  /**
   * call this method if you want to filter another serials of data
   */
  public reset() {
    this.ptr = 0;
    for (let i = 0; i < this.buffer.length; ++i) {
      this.buffer[i] = 0;
    }
  }
}
