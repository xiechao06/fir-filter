# fir-filter

[![The MIT License](https://img.shields.io/badge/license-MIT-orange.svg?style=flat-square)](http://opensource.org/licenses/MIT)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![manpm](https://img.shields.io/badge/manpm-compatible-3399ff.svg)](https://github.com/bahmutov/manpm)
[![npm](https://img.shields.io/npm/v/fir-filter.svg?style=flat-square)](https://www.npmjs.org/package/fir-filter)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

An Implementation of Finite Impulse Response filter algorithm, this implementation is the re-written of <https://sestevenson.wordpress.com/implementation-of-fir-filtering-in-c-part-1/.>

[Documentation](https://xiechao06.gitlab.io/fir-filter)

## Installation

```bash
npm i fir-filter
```

## Quick Start

```javascript
import FirFilter from 'fir-filter';

const firFilter = new FirFilter([-0.1, 0.2, 0.1]);

// filter a serials
firFilter.filter([1]);
firFilter.filter([2]);
firFilter.filter([0.5, 0.3]);

// filter another serials
firFilter.reset();
firFilter.filter([5, 4, 2, 8]);
```

## Development

```bash
# test
npm run test:watch
# build
npm run build
```
